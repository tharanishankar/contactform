<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::latest()->paginate(5);
        return view('index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'phone_number'=>'required|numeric|min:10',
            'email'=>'required|email|unique:users',
            'location'=>'required'
        ]);

        $contact = User::create($request->all());
        
        if(isset($contact)){
            // $sender = env('MAIL_USERNAME');
            $sender = 'tharanmca1@gmail.com';
            $to_name = $request->name;
            $to_email = $request->email;
            dd($sender);
            $data = array('name'=>$to_name, 'body' => 'Welcome mail');
            Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email,$sender) {
            $message->to($to_email, $to_name)->subject('Welcome Mail');
            $message->from($sender,'Contact Form');
            });
        }

        return redirect('/contacts')->with('success', 'Contact updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = User::find($id);
        return view('contact_form', compact('contact','view'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = User::find($id);
        return view('contact_form', compact('contact'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // //
        // dd($request->all());
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'phone_number'=>'required|numeric|min:10',
            'email'=>'required|email|unique:users,email,'.$id,
            'location'=>'required'
        ]);

        $contact = User::find($id);
        $contact->update($request->all());
        $contact->save();

        return redirect('/contacts')->with('success', 'Contact updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = User::find($id);
        $contact->delete();

        return redirect('/contacts')->with('success', 'Contact deleted!');
    }
}
