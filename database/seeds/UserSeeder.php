<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $contacts = [
            [
               'first_name'=>'tharani',
               'last_name'=>'priya',
               'email'=>'tharani@mail.com',
               'phone_number'=>'9876548733',
               'location'=>'Coimbatore, mettupalaiyam',
            ],
            [
                'first_name'=>'priya',
                'last_name'=>'tharani',
                'email'=>'priya@mail.com',
                'phone_number'=>'9876548732',
                'location'=>'Coimbatore, ukkadam',
            ],
            [
                'first_name'=>'Shankar',
                'last_name'=>'tharani',
                'email'=>'shankar@mail.com',
                'phone_number'=>'9876548732',
                'location'=>'Chennai, mettupalaiyam',
            ],
            [
                'first_name'=>'john',
                'last_name'=>'son',
                'email'=>'johnson@mail.com',
                'phone_number'=>'9876548732',
                'location'=>'Coimbatore, mettupalaiyam',
            ],
            [
                'first_name'=>'john',
                'last_name'=>'mathew',
                'email'=>'john@mail.com',
                'phone_number'=>'9876548732',
                'location'=>'Coimbatore, singanallur',
            ],
        ];

        foreach ($contacts as $key => $value) {
            User::create($value);
        }
    }
}
