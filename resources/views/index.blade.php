@extends('layouts.header')

@section('content')
<div class="row">
<div class="col-sm-12">
    <br>
    <div class="row">
        <div class="col-sm-6">
            <h3>Contacts Form</h3>
        </div>
        <div class="col-sm-6 text-right">
            <a href="{{ route('contacts.create') }}" class="btn btn-primary">Add Contact</a> 
        </div>
    </div>  
    </br> 
    <table class="table table-striped">
        <thead>
            <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Email</td>
            <td>Phone Number</td>
            <td>Location</td>
            <td colspan = 2>Actions</td>
            </tr>
        </thead>
        <tbody>
        @php $i=1; @endphp
            @foreach($data as $contact)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$contact->first_name}} {{$contact->last_name}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->mobile_number}}</td>
                <td>{{$contact->location}}</td>
                <td>
                    <a href="{{ route('contacts.edit',$contact->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    <form action="{{ route('contacts.destroy', $contact->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
</div>
@endsection
