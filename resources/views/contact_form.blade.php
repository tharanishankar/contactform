@extends('layouts.header')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2"><br>
    <h3 class="text-center">Add a contact</h3>
  <div>
  </br>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      @if(isset($contact))
        <form action="{{ route('contacts.update', $contact->id) }}"  method="post">
        @method('PATCH')
      @else
        <form method="post" action="{{ route('contacts.store') }}">
      @endif
          @csrf
          <div class="form-group">    
              <label for="first_name">First Name:</label>
              <input type="text" class="form-control" name="first_name" value="@if(old('first_name')){{ old('first_name') }} @else{{$contact->first_name??''}} @endif" required/>
          </div>

          <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input type="text" class="form-control" name="last_name" value="@if(old('last_name')){{ old('last_name') }} @else {{$contact->last_name??''}} @endif" required/>
          </div>

          <div class="form-group">
            <label for="last_name">Phone Number:</label>
            <input type="text" class="form-control" name="phone_number" value="@if(old('phone_number')){{ old('phone_number') }} @else {{$contact->phone_number??''}} @endif" required/>
          </div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="text" class="form-control" name="email" value="@if(old('email')){{ old('email') }} @else {{$contact->email??''}} @endif" required/>
          </div>
          <div class="form-group">
              <label for="location">Location:</label>
              <input type="text" class="form-control" name="location" id="location" value="@if(old('location')){{ old('location') }} @else {{$contact->location??''}} @endif"/>
          </div>         
          @if(!isset($view))          
          <button type="submit" class="btn btn-success">Save Contact</button>
          @endif
          <a href="{{ route('contacts.index') }}" class="btn btn-primary"> << Back</a>
      </form>
  </div>
</div>
</div>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCGX6aGjOeMptlBNc0WF3vhm0SPMl1vNBE&libraries=places&callback=initAutocomplete" type="text/javascript"></script>
<script>
       google.maps.event.addDomListener(window, 'load', initialize);

       function initialize() {
           var input = document.getElementById('location');
           var autocomplete = new google.maps.places.Autocomplete(input);
           autocomplete.addListener();
       }
    </script>
@endsection